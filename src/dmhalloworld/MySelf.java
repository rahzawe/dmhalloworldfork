package dmhalloworld;

public class MySelf {
	
	private int age;
	private String firstname;
	private String surname;
	private String goal;
	
	
	
	public MySelf() {
		age = 2;
		firstname = "HR";
		goal = "Leading IT solutions developer";
		surname = "DM";		
		
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		
		return age;

	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		
		return firstname;
	}
	/**
	 * @return the goal
	 */
	public String getGoal() {
		
		return goal;
	}
	/**
	 * @param surname the surname to set
	 */
	public String getSurname() {
		
		return surname;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MySelf [age=" + age + ", firstname=" + firstname + ", surname=" + surname + ", goal=" + goal + "]";
	}
	

}
